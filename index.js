import React from 'react';
import {
    View,
    Text,
    Modal,
    TouchableNativeFeedback,
    Dimensions,
} from "react-native";
import style from './style';


export class DarkModal extends React.Component {
    top = (Dimensions.get("screen").height - (this.props.height || 210)) / 2
    left = (Dimensions.get("screen").width - (this.props.width || 210)) / 2
    render() {
        return(
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
            >
                <View
                    style={this.props.style || {
                        width: this.props.width || 210,
                        top: this.top,
                        height: this.props.height || 140,
                        left: this.left,
                        elevation: 300,
                        borderRadius: 20,
                        alignItems: "center",
                        backgroundColor: "#202125",
                           justifyContent: "space-around",
                    }}
                >
                    <Text style={[style.Text, {fontSize: 20}]}>{this.props.title}</Text>
                    <Text style={[style.Text, {fontSize: 17}]}>{this.props.text}</Text>
                        <TouchableNativeFeedback onPress={() => {this.props.onPress}}>
                            <Text style={[style.Text, {margin: 0, marginTop: 10, marginHorizontal: 10}]}>{this.props.buttonText}</Text>
                        </TouchableNativeFeedback>
                </View>
            </Modal>
        )
    }
}

export class DarkModalCustom extends React.Component {
    top = (Dimensions.get("screen").height - (this.props.height || 210)) /2
    left = (Dimensions.get("screen").width - (this.props.width || 140)) / 2
    render() {
        return(
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
            >
                <View
                    style={this.props.style || {
                        width: this.props.width || 210,
                        top: this.top,
                        height: this.props.height || 140,
                        left: this.left,
                        elevation: 300,
                        borderRadius: 20,
                        alignItems: "center",
                        backgroundColor: "#202125",
                        justifyContent: "space-around",
                    }}
                >
                    {this.props.children}
                </View>
            </Modal>
        )
    }
}